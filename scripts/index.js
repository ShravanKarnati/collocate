
///// UI CONTROLLER

 var UIController = (function(){

    var headerMonthYear = function(){
        var d = new Date();
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";

        return {
            month : month[d.getMonth()],
            year  : d.getFullYear()
        }
    }

    var addExpense = function(id,desc,val){

    };

    var DOMStrings = {
        inputType : document.querySelector(".adder_dropbox--type"),
        inputDescription : document.querySelector(".adder_description--input"),
        inputValue : document.querySelector(".adder_value--input"),
        addBtn : document.querySelector(".adder_button"),
    };

    var formatPrice = function(price){
        price = parseFloat(price).toFixed(2);
        return price;
        // if(price > 0){
        //     return ("+" + price.toString());
        // }
        // return ("-" + price.toString())
    };
    
    return {
        headerMonth : headerMonthYear,
        adderSection : function(){
            return {
                type : DOMStrings.inputType.value,
                description : DOMStrings.inputDescription.value,
                value : parseFloat(DOMStrings.inputValue.value),
                adderButton : DOMStrings.addBtn,
            };
        },
        addItemUI : function(type,id,desc,val){
            var element;

            htmlCode = '<li class = "descript" id = "&id&-%id%"><span class="descript-text">%desc%</span><span class="descript-price">%val%</span><div class="descript-delete">Delete</div></li>'
            
            type == 'ear' ? element = ".descriptor_earnings--lists" : element = ".descriptor_expenses--lists";
            type == 'ear' ? htmlCode = htmlCode.replace("&id&","ear") : htmlCode = htmlCode.replace("&id&","exp");
            
            htmlCode = htmlCode.replace("%id%",id);
            htmlCode = htmlCode.replace("%desc%",desc);
            htmlCode = htmlCode.replace("%val%",formatPrice(val));

            document.querySelector(element).insertAdjacentHTML("beforeend",htmlCode);

            var fields = document.querySelectorAll(".adder_description--input,.adder_value--input");

            fields.forEach(function(e){
                e.value = "";
            });

            fields[0].focus();
        },

        changeBudgetUI : function(type,value){
            var element;
            type == 'ear' ? element = ".header_earnings--amount" : element = ".header_expenses--amount";

            document.querySelector(element).textContent = (formatPrice(value));//.slice(1));
        },

        changeTotal : function(obj){
            var total = obj.ear - obj.exp ;

            document.querySelector(".header_total--amount").textContent = formatPrice(total);

        },

        deleteItemUI : function(eventID){

            var child = document.getElementById(eventID);
            child.parentNode.removeChild(child);
        },

        testing : function(){
            var fields = document.querySelectorAll(".adder_description--input,.adder_value--input");
            return fields;
        }
        };
 })();

///// BUDGET CONTROLLER
var BudgetController = (function(){

    class Expense {
        constructor(id, description, value) {
            this.id = id;
            this.description = description;
            this.value = value;
        }
    }

    class Earning {
        constructor(id, description, value) {
            this.id = id;
            this.description = description;
            this.value = value;
        }
    }

    var data = {
        allItems : {
            exp : [],
            ear : [],
        },
        total : {
            exp:0,
            ear:0,
        },
    };

    return {
        addItem : function(ty,des,val){
            var newItem;

            id = 0;

            if(data.allItems[ty]['length']){
                id = data.allItems[ty][data.allItems[ty]['length'] - 1].id + 1;
            }

            ty == "ear" ? newItem = new Earning(id,des,val) : newItem = new Expense(id,des,val);

            data.allItems[ty].push(newItem);
            return newItem;

        },

        deleteItem : function(type,Id){

            var dataCurrent = data.allItems[type];

            var array = dataCurrent.map(function(current){
                return current.id;
            });

            Id = parseInt(Id);

            index = array.indexOf(Id);

            value = data.allItems[type][index].value;

            if(index !== -1){
                data.allItems[type].splice(index,1);
            }

            return value;
        },
        
        addBudget : function(type,value){
            data.total[type] = data.total[type] + value;
            return data.total[type];
        },

        deleteBudget : function(type,value){
            data.total[type] = data.total[type] - value;
            return data.total[type];
        },

        totalAmount : function(){
            return {
                ear : data.total.ear,
                exp : data.total.exp,
            }
        },

        testing:function(){
            console.log(data);
            // return data;
        }
    }

})();

///// GLOBAL CONTROLLER
var controller = (function(UCtrl,BCtrl){

    var UpdateBudget = function(type,value,fun){
        if(fun === "add"){
            totalBudget = BCtrl.addBudget(type,value);
        }
        else{
            totalBudget = BCtrl.deleteBudget(type,value);
        }
        UCtrl.changeBudgetUI(type,totalBudget);
        UCtrl.changeTotal(BCtrl.totalAmount());
    };

    var ctrlAddItem = function(){

        var type = UCtrl.adderSection().type;
        var description = UCtrl.adderSection().description;
        var value = UCtrl.adderSection().value;

        if (!(description != "" && !isNaN(value) && value > 0)) return;
        var newItem = BCtrl.addItem(type,description,value);

        UCtrl.addItemUI(type,newItem.id,newItem.description,newItem.value);

        UpdateBudget(type,value,"add");
    }

    var delegationItem = function(){
        document.querySelector(".descriptor").addEventListener('click',function(event){
            var eventID = (event.target.parentNode.id);

            if(!(eventID)) return;
            
            var eleType = (eventID.split("-"))[0];
            var eleID = (eventID.split("-"))[1];

            var value = BCtrl.deleteItem(eleType,eleID);

            UCtrl.deleteItemUI(eventID);

            UpdateBudget(eleType,value,"del");
        });
    };

    return {
        init : function(){

            document.querySelector('.header_month--month').textContent =UCtrl.headerMonth().month;
            document.querySelector('.header_month--year').textContent = UCtrl.headerMonth().year;

            document.addEventListener("keypress",function(event){
                if (event.keyCode == 13 || event.which == 13){
                    ctrlAddItem();
                }
            })

            UCtrl.adderSection().adderButton.addEventListener("click",function(){
                ctrlAddItem();
            })

            delegationItem();
        }
    };
})(UIController,BudgetController);


controller.init();